/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stddef.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define BUTTON_PRESS_STATE 4
#define DELAY_TIME 2000
#define BASE_ITERATION 4
#define BUTTONS_AMOUNT 4
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
typedef enum{
  RELEASED,
  PRESSED
}btn_state_t;
typedef void (*callback)(void);
typedef struct{
  GPIO_TypeDef * port;
  uint8_t pin;
  btn_state_t state;
  uint8_t counter;
  callback cb;
}button_t;


void Button_1_action(void);
void Button_2_action(void);
void Button_3_action(void);
void Button_4_action(void);
static button_t buttons[BUTTONS_AMOUNT] = {
    {.port =btn_1_GPIO_Port, .pin =btn_1_Pin, .state =RELEASED, .counter=0, .cb=Button_1_action},
    {.port =btn_2_GPIO_Port, .pin =btn_2_Pin, .state =RELEASED, .counter=0, .cb=Button_2_action},
    {.port =btn_3_GPIO_Port, .pin =btn_3_Pin, .state =RELEASED, .counter=0, .cb=Button_3_action},
    {.port =btn_4_GPIO_Port, .pin =btn_4_Pin, .state =RELEASED, .counter=0, .cb=Button_4_action}
};

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* USER CODE BEGIN PV */
osThreadId_t btn_detection_state_task_handler;
const osThreadAttr_t btn_detection_state_task_attributes = {
  .name = "defaultTask",
  .priority = (osPriority_t) osPriorityNormal1,
  .stack_size = 128 * 4
};
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
void StartDefaultTask(void *argument);

/* USER CODE BEGIN PFP */
void btn_detection_state_task(void *argument);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  btn_detection_state_task_handler = osThreadNew(btn_detection_state_task, NULL, &btn_detection_state_task_attributes);
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();
 
  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 120;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, led_1_Pin|led_2_Pin|led_3_Pin|led_4_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : led_1_Pin led_2_Pin led_3_Pin led_4_Pin */
  GPIO_InitStruct.Pin = led_1_Pin|led_2_Pin|led_3_Pin|led_4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : btn_1_Pin btn_2_Pin */
  GPIO_InitStruct.Pin = btn_1_Pin|btn_2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : btn_3_Pin btn_4_Pin */
  GPIO_InitStruct.Pin = btn_3_Pin|btn_4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void btn_detection_state_task(void *argument){
  for(;;){

    for(size_t i = 0; i<BUTTONS_AMOUNT; i++){
      if(!HAL_GPIO_ReadPin(buttons[i].port,buttons[i].pin)){
        if(buttons[i].counter<=BUTTON_PRESS_STATE){
          buttons[i].counter++;
        }else {
          buttons[i].counter=BUTTON_PRESS_STATE;
          buttons[i].state=PRESSED;
         }
      }else {
        buttons[i].counter=0;
        buttons[i].state=RELEASED;
      }
    }
    osDelay(10);
  }
}
/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
void Button_1_action(void){
  HAL_GPIO_TogglePin(led_1_GPIO_Port, led_1_Pin);
};
void Button_2_action(void){
  HAL_GPIO_TogglePin(led_2_GPIO_Port, led_2_Pin);
};
void Button_3_action(void){
  HAL_GPIO_TogglePin(led_3_GPIO_Port, led_3_Pin);
};
void Button_4_action(void){
  HAL_GPIO_WritePin(led_1_GPIO_Port, led_1_Pin,GPIO_PIN_SET);
  osDelay(DELAY_TIME);
  HAL_GPIO_WritePin(led_2_GPIO_Port, led_2_Pin,GPIO_PIN_SET);
  osDelay(DELAY_TIME);
  HAL_GPIO_WritePin(led_3_GPIO_Port, led_3_Pin,GPIO_PIN_SET);
  osDelay(DELAY_TIME);
  HAL_GPIO_WritePin(led_4_GPIO_Port, led_4_Pin,GPIO_PIN_SET);
  osDelay(DELAY_TIME);
  HAL_GPIO_WritePin(led_1_GPIO_Port, led_1_Pin,GPIO_PIN_RESET);
  HAL_GPIO_WritePin(led_2_GPIO_Port, led_2_Pin,GPIO_PIN_RESET);
  HAL_GPIO_WritePin(led_3_GPIO_Port, led_3_Pin,GPIO_PIN_RESET);
  HAL_GPIO_WritePin(led_4_GPIO_Port, led_4_Pin,GPIO_PIN_RESET);
};
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN 5 */
  for(uint8_t i = 0; i < BASE_ITERATION; i++){
    HAL_GPIO_WritePin(led_2_GPIO_Port, led_2_Pin,GPIO_PIN_SET);
    osDelay(DELAY_TIME);
    HAL_GPIO_WritePin(led_2_GPIO_Port, led_2_Pin,GPIO_PIN_RESET);
    osDelay(DELAY_TIME);
  }

  /* Infinite loop */
  for(;;)
  {
    for(uint8_t i = 0; i<BUTTONS_AMOUNT; i++){
      if(buttons[i].state && buttons[i].cb!=NULL){
        buttons[i].cb();
      }
    }

    osDelay(10);
  }
  /* USER CODE END 5 */ 
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
